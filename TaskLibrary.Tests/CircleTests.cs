﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskLibrary.GeometricShapes;

namespace TaskLibrary.Tests
{
    [TestClass]
    public class CircleTests
    {
        private readonly double radius;

        public CircleTests()
        {
            this.radius = 4.0;
        }

        #region ExpectedException
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Circle_Null_ArgumentNullException()
        {
            //Arrange
            const string name = null;

            //Act
            Circle circle = new Circle(name, radius);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Circle_Empty_ArgumentNullException()
        {
            //Arrange
            const string name = "";

            //Act
            Circle circle = new Circle(name, radius);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Circle_Space_ArgumentNullException()
        {
            //Arrange
            const string name = " ";

            //Act
            Circle circle = new Circle(name, radius);
        } 
        #endregion

        [TestMethod]
        public void Area_4radius()
        {
            //Arange
            const double expected = 50.26548245743669;

            //Act
            Circle circle = new Circle(nameof(Circle), radius);
            double actual = circle.Area();

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
