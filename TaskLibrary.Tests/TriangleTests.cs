﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskLibrary.GeometricShapes;

namespace TaskLibrary.Tests
{
    [TestClass]
    public class TriangleTests
    {
        private double legOne, legTwo, legThree;

        public TriangleTests()
        {
            this.legOne = 4.0;
            this.legTwo = 5.0;
            this.legThree = 3.0;
        }

        #region ExpectedException
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Triangle_Null_ArgumentNullException()
        {
            //Arrange
            const string name = null;

            //Act
            Triangle triangle = new Triangle(name, legOne, legTwo, legThree);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Triangle_Empty_ArgumentNullException()
        {
            //Arrange
            const string name = "";

            //Act
            Triangle triangle = new Triangle(name, legOne, legTwo, legThree);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Triangle_Space_ArgumentNullException()
        {
            //Arrange
            const string name = " ";

            //Act
            Triangle triangle = new Triangle(name, legOne, legTwo, legThree);
        } 
        #endregion

        [TestMethod]
        public void IsRightTriangle_4and5and3_trueReturned()
        {
            //Arange
            const bool expected = true;

            //Act
            Triangle triangle = new Triangle(nameof(Triangle), legOne, legTwo, legThree);
            bool actual = triangle.IsRightTriangle();

            //Assert
            Assert.AreEqual(expected, actual);
        }

    }
}
