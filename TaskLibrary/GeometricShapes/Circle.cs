﻿using System;

namespace TaskLibrary.GeometricShapes
{
    public class Circle : Shape
    {
        private double radius;

        public Circle(string name, double radius)
            : base(name)
        {
            this.radius = radius < 0 ? -radius : radius;
        }

        public override double Area()
        {
            return Math.PI * Math.Pow(radius, 2.0);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
