﻿using System;

namespace TaskLibrary.GeometricShapes
{
    public abstract class Shape
    {
        public string Name { get; }

        protected Shape(string nameShape)
        {
            if (string.IsNullOrWhiteSpace(nameShape))
            {
                throw new ArgumentNullException(nameof(nameShape));
            }

            Name = nameShape;
        }

        public abstract double Area();
    }
}
