﻿using System;

namespace TaskLibrary.GeometricShapes
{
    public class Triangle : Shape
    {
        private double legOne, legTwo, legThree;

        public Triangle(string name, double legOne, double legTwo, double legThree)
            : base(name)
        {
            this.legOne = legOne < 0 ? -legOne : legOne;
            this.legTwo = legTwo < 0 ? -legTwo : legTwo;
            this.legThree = legThree < 0 ? -legThree : legThree;
        }

        public bool IsRightTriangle()
        {
            return Math.Pow(legOne, 2.0) + Math.Pow(legTwo, 2.0) == Math.Pow(legThree, 2.0)
                || Math.Pow(legTwo, 2.0) + Math.Pow(legThree, 2.0) == Math.Pow(legOne, 2.0)
                || Math.Pow(legThree, 2.0) + Math.Pow(legOne, 2.0) == Math.Pow(legTwo, 2.0);
        }
        
        public override double Area()
        {
            double semiPerimeter = (legOne + legTwo + legThree) / 2.0;

            return Math.Sqrt(semiPerimeter * (semiPerimeter - legOne)
                * (semiPerimeter - legTwo) * (semiPerimeter - legThree));
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
