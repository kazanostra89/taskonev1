﻿using System;
using System.Collections.Generic;

namespace TaskLibrary.Interfaces
{
    /// <summary>
    /// Для собственной реализации вычисления площади фигуры
    /// </summary>
    interface IFigureArea
    {
        double FigureArea(Dictionary<string, object> parameters);
    }
}
